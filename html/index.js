const getProducts = () => {
  fetch("http://localhost:3000/products")
    .then((response) => response.json())
    .then((result) => {
      const data = result.data;
      console.log(result.data);
      const tableProduct = document.getElementById("table-product");
      let content = "";
      data.forEach((product) => {
        content += `
        <tr>
            <td
            class="text-left font-thin py-2 px-2 bg-[#F3F6FF] border-b border-l border-[#E8E8E8]"
            >
            ${product.name}
            </td>
            <td
            class="text-center font-thin py-2 px-2 bg-white border-b border-[#E8E8E8]"
            >
            ${product.price}
            </td>
            <td
            class="text-center font-thin py-2 px-2 bg-white border-b border-r border-[#E8E8E8]"
            >
            <a
                href="#"
                onclick="detailProduct(${product.id})"
                class="text-sm border border-primary py-2 px-6 text-primary inline-block rounded hover:bg-indigo-600 hover:text-white"
            >
                Detail
            </a>
            <a
                href="#"
                onclick="editProduct(${product.id}, '${product.name}', ${product.price})"
                class="text-sm border border-primary py-2 px-6 text-primary inline-block rounded hover:bg-indigo-600 hover:text-white"
            >
                Edit
            </a>
            <a
                href="#"
                onclick="deleteProduct(${product.id})"
                class="text-sm border border-primary py-2 px-6 text-primary inline-block rounded hover:bg-red-500 hover:text-white"
            >
                Delete
            </a>
            </td>
        </tr>`;
      });
      tableProduct.innerHTML = content;
    })
    .catch((err) => console.log("err", err));
};

getProducts();

const addProduct = () => {
  const name = document.getElementById("form-name").value;
  const price = document.getElementById("form-price").value;
  //   alert("test");
  fetch("http://localhost:3000/product", {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify({
      name,
      price,
    }),
  })
    .then((res) => {
      alert("berhasil menambahkan data");
    })
    .catch((err) => {
      console.log("err", err);
    });
};

const deleteProduct = (id) => {
  fetch("http://localhost:3000/product/" + id, {
    method: "DELETE",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => {
      alert("berhasil menghapus data");
    })
    .catch((err) => {
      console.log("err", err);
    });
};

const editProduct = (id, name, price) => {
  const formName = document.getElementById("form-name");
  const formPrice = document.getElementById("form-price");
  const btnSave = document.getElementById("btn-save");
  formName.value = name;
  formPrice.value = price;

  btnSave.setAttribute("onclick", `saveProduct(${id})`);
};

const saveProduct = (id) => {
  const formName = document.getElementById("form-name");
  const formPrice = document.getElementById("form-price");
  const btnSave = document.getElementById("btn-save");

  const name = formName.value;
  const price = formPrice.value;

  fetch("http://localhost:3000/product/" + id, {
    method: "PUT",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ name, price }),
  })
    .then((res) => {
      alert("berhasil mengubah data");
      formName.value = "";
      formPrice.value = "";
      btnSave.setAttribute("onclick", "addProduct()");
    })
    .catch((err) => {
      console.log("err", err);
    });
};

const detailProduct = (id) => {
  fetch("http://localhost:3000/product/" + id)
    .then((response) => response.json())
    .then((result) => {
      alert(
        `Detail Product \n\n Name  : ${result.data.name} \n Price : ${result.data.price}`
      );
    })
    .catch((err) => console.log("err", err));
};
