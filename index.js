const express = require("express");
var cors = require("cors");
const app = express();

app.use(cors());

const DATABASE = "./db/products.json";
const dataProducts = require("./db/products.json");
const fs = require("fs");

var bodyParser = require("body-parser");
const { json } = require("body-parser");

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

function isNumber(n) {
  return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
}

app.get("/products", function (req, res) {
  fs.readFile(DATABASE, "utf-8", (err, products) => {
    if (err) {
      res.status(400).json({
        message: "Terjadi kesalahan pada API",
      });
    } else {
      res.status(200).json({
        data: JSON.parse(products),
      });
    }
  });
});

app.get("/product/:id", (req, res) => {
  const id = req.params.id;

  if (isNumber(id)) {
    fs.readFile(DATABASE, "utf-8", (err, data) => {
      const products = JSON.parse(data);
      const product = products.filter((d) => d.id == id);
      if (products.length) {
        res.json({
          message: "Get detail product",
          data: product[0],
        });
      } else {
        res.status(404).json({
          message: "product is not found",
        });
      }
    });
  } else {
    res.status(400).json({
      message: "Please input id with number",
    });
  }
});

app.post("/product", (req, res) => {
  const { name, price } = req.body;

  if (!name || !price) {
    res.status(400).json({
      message: "Please fulfill all required input",
    });
  } else {
    fs.readFile(DATABASE, "utf-8", (err, data) => {
      const products = JSON.parse(data);
      const newProduct = {
        id: products[products.length - 1].id + 1,
        ...req.body,
      };
      products.push(newProduct);
      fs.writeFile(DATABASE, JSON.stringify(products), (err) => {
        res.status(201).json({
          message: "Succesfully create data",
          data: newProduct,
        });
      });
    });
  }
});

app.put("/product/:id", (req, res) => {
  let products = dataProducts;
  let id = req.params.id;
  let { name, price } = req.body;
  let idFound = false;
  products.forEach((product) => {
    if (product.id == id) {
      product.name = name;
      product.price = price;
      idFound = id;
      return;
    }
  });
  if (idFound) {
    fs.writeFile(DATABASE, JSON.stringify(products), (err) => {
      res.status(200).json({
        message: "Succesfully updated data",
        data: {
          name,
          price,
          id: idFound,
        },
      });
    });
  } else {
    res.status(404).json({
      message: "Data not found in our database",
    });
  }
});

app.delete("/product/:id", (req, res) => {
  const products = dataProducts;
  const id = req.params.id;
  let filterData = products.filter((product) => product.id != id);
  const rowsChanged = products.length - filterData.length;
  fs.writeFile(DATABASE, JSON.stringify(filterData), (err) => {
    res.json({
      message: "Successfully deleted data",
      rowsChanged,
    });
  });
});

app.listen(3000, () => {
  console.log("listening on port 3000");
});
